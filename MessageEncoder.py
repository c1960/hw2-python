#!/bin/python3

# CS 472 - HW2
# Christopher S. Good, Jr. 
# MessageEncoder.py

import re

class MessageEncoder:

    def __init__(self, encoding='UTF-8', terminator=b'\r\n'):
        self.encoding = encoding
        self.terminator = terminator

    def encode(self, message):
        '''
        Encodes the given message to the set encoding

        Parameters
        ----------
        `message`: str => message to be converted

        Return
        ------
        `message`: bytearray => converted message
        '''
        return message.encode(self.encoding) + self.terminator

    def decode(self, message):
        '''
        Decodes the given message from a UTF-8 byte array and strips the newline

        Paramters
        ---------
        `message`: bytearray => message to be converted

        Return 
        ------
        `message`: str => converted message
        '''
        return message.decode(self.encoding)

    def capture_code(self, message):
        '''
        Captures the first occurrence of a 3 digit code from a message

        Paramters
        ---------
        `message`: str => message to capture code from

        Return
        ------
        `code`: int => captured code from message
        '''
        return int(re.findall('[0-9]{3}', message)[0])
