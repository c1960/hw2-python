import re
from socket import gethostbyname

def handleUserAddressInput(userIn):
    '''
    Conforms the user's input to a consumable IP address
    @param userIn: ip or host string
    @return ip address (string)
    '''
    # TODO: Add error handling to his helper to fail fast
    # Regex to capture the IP format (XXX.XXX.XXX.XXX)
    expr = re.compile('/\[0-9]{3}.\[0-9]{3}.\[0-9]{3}.\[0-9]{3}\/')
    if expr.match(userIn):
        return userIn
    else:
        try:
            return gethostbyname(userIn.encode('UTF-8'))
        except:
            return -1