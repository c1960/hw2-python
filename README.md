# CS 472 - HW2
Christopher S. Good, Jr. 
FTP Client Program
Python 3
Assigned: September 29, 2021
Completed: October 13, 2021

# Project Structure
This program is broken up into 4 main parts:
- main.py:
    This is the main function that drives the program and handles user input
- FtpClient.py:
    Wrapper for the Socket Library with built in commands for interacting with an FTP Server
- MessageEncoder.py:
    Message handler that focuses on encoding and decoding message traffic to and from the FTP Server
- Logger.py:
    Custom Logger that directs program output to either a file, the screen, or both
- AddressHandler.py:
    Custom Address Handler function that matches IPv4 Addresses and calculates IP Addresses from Host Names
- SampleLogs:
    Collection of sample log txt files that contain output from various tests of the system

# Running the Program
To run the program please provide (at the least) an IP Address of an FTP Server
and run the following command
`python3 main.py -a XXX.XXX.XXX.XXX`
or
`./main.py -a XXX.XXX.XXX.XXX`

The program is designed to accept arguments after a given flag (with a space!)
Flags:
- a: IP Address of the FTP Server to connect to
- lf: Name of the desired Log File

Additionally, there is a developer "Test" flag build into the code, that makes it easier 
to get up and running on tux:
- t: runs the program in 'Test' mode, which enters the USER and PASS commands for the user

# Running the Program with Arguments
To run the progam with a specific log file name
`python3 main.py -a XXX.XXX.XXX.XXX -lf your-log-file.txt`
or 
`./main.py -a XXX.XXX.XXX.XXX -lf your-log-file.txt`

# FTP Client Wrapper
The FTP Client Wrapper is features various methods that aid in interacting with the FTP Server
- Core:
    - __init__: Constructs the class and defines the initial Command Socket
    - __del__: Handles closing all sockets when the class leaves scope or program exits
    - sendCmd: Specialized method that handles messages being sent over the Command Socket
    - sendData: Specialized method that handles messages being sent over the Data/File Socket
    - send: Base message method class
    - recvCmd: Specialized method that handles data being received over the Command Socket
    - recvData: Specialized method that handles data being received over the Data/File Socket
    - reply: Base data receiving method
    - connect: Handles the initial connection of the Command Socket to the FTP Server 
- FTP Methods:
    - user: Calls the USER command => Control Socket
    - password: Calls the PASS command => Control Socket
    - cwd: Calls the CWD command => Control Socket
    - pwd: Calls the PWD command => Control Socket
    - list: Calls the LIST command => Transfers over Data Socket
    - pasv: Calls the PASV command => Creates Data Socket
    - epsv: Calls the EPSV command => Creates Data Socket
    - port: Calls the PORT command => Creates Data Socket
    - eprt: Calls the EPRT command => Creates Data Socket
    - stor: Calls the STOR command => Transfers over Data Socket
    - retr: Calls the RETR command => Transfers Data Socket
    - syst: Calls the SYST command => Control Socket
    - help: Calls the HELP command => Control Socket

# Questions

## Question 1
When the program starts, the user begins by passing in the address of the FTP Server that they would like
to connect to. This is what then starts the connection between the Client and Server over the Control Socket, 
which is the data channel that handles all of the Commands passed between the Client and Server. This Control
data channel serves as the method of communication between the two parties, essentially this channel and connection
is how the Client knows who the Server is, and vice-versa. 

Further more, near the beginning of the interaction, the client has to provide authentication in the form of a 
username and password to the Server in order to gain access to the contents. This user/pass combo is really the 
only layer of security that FTP has (or that I was able to find) and is how it validates that the client should 
have access to the Server. In terms of the process, the client sends a username and awaits for an OKAY response 
that also has a bit set asking for a password. From there, when the password is provided, the Server provides 
a positive return code (200's) if the client can access or a `negative` return code that says they are not logged in.
From here, the client has to handle this logged-in state if it wants to either quit or try to log in again. 

Finally, as the program is executing, the Client takes the burden of the operations and sends an initial code to the Server, 
where it will either tell or ask something of the Server. For example, when running the PASV and PORT commands, 
PASV will ask for an address to connect to and the Server will provide it to the client (with a little math involved).
On the other hand, with PORT the client is telling the server to connect on a specific port (and address) in order to 
transfer the files. In terms of trusting the communication, the entire premise is based on only one person being at 
the other end of the socket that each party asks/tells the other to open. So when the client uses PASV to ask for an 
address and port, the server is assuming that the ONLY the client will be listening on that specific port (however, this
may not always be the case!)

## Question 2
While the Client is sending the commands over to the Server, the order is roughly interpreted as Client -> Server, then 
Server -> Client. Continuing with the PASV example, the Client will send a command to the Server saying `Send me an address
and port pair that you have open`, to which the server replies `Here is that port and a fun math problem`. Additionally, 
each of these messages has a corresponding `Return Code`, which related to the status of the message or the state that the
conversation is in. For instance, when running the LIST command, the Client sends the command to the Server, and the Server
responds by saying `150 Here comes the directory`, which prompts the Client to expect to read from the Data Socket (Rather
than from the Control Socket). Then, after the EOF is reached on the data being passed over, the Server sends another message
over the Control Socket saying `226 Directory send OK.`.

When running the program similarly to the way I was describing above, you will notice that each command prompts a response, 
and that response carries with it a `state`, which tells you how the conversation is going. When the conversation is going
off course or is getting out of order, the Server will tell the Client that with a special return code. For the FTP Protocol, these returns are typically in the 500 range. Now, going off what I said earlier about the Client carrying the 
burden of the connections and commands, it is the Server's responsibility to keep the state of the conversation in order,
since the Client can really send whatever it wants. On the `backend`, the Server is keeping track of the order of the 
conversation through the various DFAs that spell out the paths for these calls. 

Finally, the client really does not know that the FTP Server is trustworthy, aside from the fact that it hopefully grabbed
the address from someone/somewhere that was. At the end of the day, the Server could fake the authentication and just 
make it seem like everyone is a user and provide malicious files. This burden, falls on the Client to make sure that it is
connecting to the proper address and port pairs, and that they came from the proper places/people.