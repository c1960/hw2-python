#!/usr/bin/python3

# CS 472 - HW2
# Christopher S. Good, Jr.
# FtpClient.py

# Handle the Default Timeout Values
DEFAULT_COMMAND_TIMEOUT = 4
DEFAULT_DATA_TIMEOUT = 20

import os
import re
from socket import has_dualstack_ipv6, socket, create_server, \
    gethostbyaddr, getaddrinfo, error, timeout, AF_INET, AF_INET6, SOCK_STREAM, SHUT_RDWR
from Logger import Logger
from MessageEncoder import MessageEncoder
from AddressHandler import handleUserAddressInput

class FtpClient:
    '''
    FTP Client Socket Wrapper

    This class wraps the implementation of the FTP Client connection so that the control flow 
    can be abstracted away from what's 'under the hood'
    '''

    def __init__(self, logger=Logger('ftp-logger.txt'), encoder=MessageEncoder('UTF-8'), sock=socket(AF_INET, SOCK_STREAM)):
        '''
        Initializes the FTP Client Instance

        Note: Creates a new instance of a Logger and defaults socket to None

        Parameters
        ----------
        `logger`: Logger => instance of a logger (Default logfile is ftp-logger.txt)
        `encoder`: MessageEncoder => instance of a message encoder (Default is UTF-8)
        `sock`: Socket => default socket (abstracted for testing)
        '''
        self.logger = logger
        self.encoder = encoder
        self.control_sock = sock
        self.control_sock.settimeout(DEFAULT_COMMAND_TIMEOUT)
        self.data_sock = None
        self.data_sock_type = None
        self.helpCommands = [
            'username/USERNAME',
            'password/PASSWORD',
            'login/LOGIN',
            'cd/CD',
            'port/PORT',
            'extendedPort/EXTENDEDPORT',
            'passive/PASSIVE',
            'extendedPassive/EXTENDEDPASSIVE',
            'grab/GRAB',
            'ls/LS',
            'put/PUT',
            'system/SYSTEM',
            'pwd/PWD',
            'help/HELP'
        ]

    def __del__(self):
        '''Destructs the created socket when this class leaves scope'''
        if self.control_sock:
            self.control_sock.close()
        if self.data_sock:
            self.data_sock.close()

    def checkControlSock(self):
        '''Helper function that prints an analysis of the current socket'''
        if self.control_sock:
            self.logger.console_log('INFO', self.control_sock)
        else:
            self.logger.console_log(
                'ERROR', 'There does not appear to be a command socket established for this client...')

    def checkDataSock(self):
        '''Helper function that prints an analysis of the current file socket'''
        if self.data_sock and self.data_sock_type:
            self.logger.console_log(
                'INFO', f'Socket Type: {self.data_sock_type} Socket: {self.data_sock}')
        else:
            self.logger.console_log(
                'ERROR', 'There does not appear to be a file socket established fo this client...')

    def resetDataSock(self):
        '''Helper function that resets the data socket and data socket type to None'''
        self.logger.log('INFO', 'Attempting to reset data socket')
        if self.data_sock and self.data_sock_type:
            self.data_sock.close()
            self.data_sock = self.data_sock_type = None
            self.logger.log('SUCCESS', 'Data socket has been reset')
            try:
                code, message = self.recvCmd()
                self.logger.log('WARNING', f'Read {code}: {message}')
            except:
                self.logger.log('WARNING', 'No data to flush from Control socket')
        else:
            self.logger.log('WARNING', "There isn't a data socket available for reset")
        return 0, 'Data Socket Reset and Control Socket Buffer Cleared'

    def connect(self, addr, port=21):
        '''
        Creates a connection with an FTP server using the socket library
        Note: Receives the initial connection message from the FTP Server

        Parameters
        ----------
        `addr`: str => IP address of the FTP Server to connect to
        `port`: int => Port number to use for the FTP Communications (Default is 21)

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ------------
        `-1` => Unable to resolve the host's address or cannot connect
        `220` => Successful connection
        '''
        pair = (addr, port)
        host = None
        try:
            host = gethostbyaddr(addr)[0]
        except:
            self.logger.log('ERROR', 'Unable to resolve host!')
            # If the host cannot be resolved, return an error code
            # Note: This is most likely due to a VPN issue
            return -1, 'Unable to resolve host from address.'
        try:
            self.logger.log('INFO', f'Connecting to {host}')
            self.control_sock.connect(pair)
            connection_code, connection_message  = self.recvCmd()
            if connection_code == 220:
                self.logger.log(connection_code, connection_message)
                return connection_code, connection_message
            else:
                return -1, f'Error while connecting to {host}.'
        except error:
            return -1, f'Could not connect to {host}.'

    def sendFile(self, pathname):
        '''
        Send's the users desired file to the FTP Server by the given pathname

        Accepts both PORT and PASV connections and selects the proper channel to 
        send the data through. The file is then opened in a readonly binary state
        and is sent to the FTP Server with the `sendfile` socket method. The return of
        this action is the size of the file transfer, which is used as the func return.

        Paramters
        ---------
        `pathname`: str => name of the file to be transfered to the FTP Server

        Return
        ------
        `sent`: int => size of the data transfer
        '''
        conn = self.data_sock # Default to PASV
        if self.data_sock_type == 'PORT': # Switch on PORT
            conn, _ = self.data_sock.accept()
        # Read file as a readonly binary file    
        with open(pathname, 'rb') as binary_file:
            try:
                sent = conn.sendfile(binary_file)
                conn.shutdown(SHUT_RDWR) # Close the connection
                if sent:
                    return sent
                else:
                    return -5
            except timeout:
                return -2
            except KeyboardInterrupt:
                return 221
        

    def sendCmd(self, user_input):
        '''
        Sends the user's command over the socket to the FTP Server

        Parameters
        ----------
        `user_input`: str => User's command to send to the FTP Server

        Return
        ------
        `code`: int => status code of the send command

        Return Codes
        ------------
        `0` => Successfully sent command
        `1` => Error occurred while sending the command
        '''
        try:
            self.control_sock.sendall(self.encoder.encode(user_input))
            self.logger.log('Sent', user_input)
            return 0
        except timeout:
            self.logger.log('ERROR', 'The connection timedout')
            return -2
        except KeyboardInterrupt:
            return 221
        except:
            self.logger.log('ERROR', 'The connection to the FTP Server was lost before the message could be sent...')
            return -1

    def recvCmd(self):
        '''
        Receives the reply from the FTP Server and logs the result

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket
        '''
        msg = []
        try:
            while True:
                # Read byte by byte until a newline character is found
                m = self.control_sock.recv(1)
                if m == b'\n' or not m:
                    break
                msg.append(m)
        except timeout:
            # Return an error code if the message timed out
            return -2, 'The socket timed out while reading from the FTP Server'
        except KeyboardInterrupt:
            return -4, 'Closing socket connection'

        # Decode the segments of the message
        for i in range(len(msg)):
            msg[i] = self.encoder.decode(msg[i])

        msg = ''.join(msg) # Join the message into a readable string

        # Assert that a message was received and provide a return code and message
        if msg:
            try:
                return_code = self.encoder.capture_code(msg)
            except:
                return_code = -1

            return return_code, msg
        else:
            # Return an error code if a message was not received
            return -1, 'Something went wrong while trying to read from the FTP Server'

    def user(self, username=''):
        '''
        Runs the USER command

        Takes the provided `username` and passes it to the FTP Server to attempt a login. If the 
        login succeeds, then the user will be prompted for a password. If it fails, then the user will 
        get the error code from the FTP Server or the custom error code for lack of input.

        Parameters
        ----------
        `username`: str => the username used to log into the FTP Server 

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the FTP Server

        Return Codes
        ------------
        `230` => Good command; User already logged in
        `331` => Good username; needs password, `332` => Need an account to login
        `421` => Service not available
        `500`, `501` => Syntax or parameter error
        `530` => User not logged in
        '''
        self.sendCmd(f'USER {username}')
        username_code, username_message = self.recvCmd()
        self.logger.console_log(username_code, username_message)
        return username_code, username_message

    def password(self, password=''):
        '''
        Runs the PASS command

        Takes the provided `password` and passes it to the FTP Server to attempt a login. If the
        login succeeds, then the user will be granted access to the system and prompted to enter 
        more commands. If it fails, then the user will get the error code from the FTP Server or
        the custom error code for lack of input.

        Parameters
        ----------
        `password`: string => the password to log into the FTP Server

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ------------
        `202` => Command not implemented on Server, `230` => User already logged in
        `332` => Need account for login
        `421` => Service not available
        `500`, `501` => Syntax or parameter error
        `530` => User not logged in
        '''
        self.sendCmd(f'PASS {password}')
        password_code, password_message = self.recvCmd()
        self.logger.console_log(password_code, password_message)
        return password_code, password_message

    def cwd(self, pathname):
        '''
        Runs the CWD command

        Takes the given pathname and changes the PWD to that pathname on the FTP Server

        Parameters
        ----------
        `pathname`: string => pathname of the desired directory 

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ------------
        `250` => Request is okay; completed
        `421` => Service not available
        `500`, `501`, `502` => Syntax or parameter error
        `530` => User not logged in
        `550` => File/Location unavailable; action not taken
        '''
        cwd_code = cwd_message = None
        if pathname:
            self.sendCmd(f'CWD {pathname}')
            cwd_code, cwd_message = self.recvCmd()
        else:
            cwd_code, cwd_message = -1, 'Pleast provide a pathname'
            
        self.logger.log(cwd_code, cwd_message)
        return cwd_code, cwd_message

    def quit(self):
        '''
        Runs the QUIT command

        Serves as a 'passthrough' function for the FTP Client. The return code of this
        method will trickle down to the main process and terminate the program after displaying
        the 'Goodbye' message.

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ------------
        `221` => Service is closing
        `500` => Syntax error (unrecognized command)
        '''
        self.sendCmd('QUIT')
        quit_code, quit_message = self.recvCmd()
        self.logger.log(quit_code, quit_message)
        return quit_code, quit_message

    def pasv(self):
        '''
        Runs the PASV command

        Opens a Passive connection with the FTP Server through an IPv4 Socket. If the user
        already has a data connection open, then this method will fail and alert the user.
        When creating a connection, the method captures the designated port from the Server 
        and establishes the connection.

        Port Arithmetic: (p1 * 256) + p2 = data port 

        Note: This implementation supports PASV toggling (Alerts the user to open data channels)
        
        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ------------
        `-1` => Error with Python Socket Connection or command
        `227` => Entering passive mode
        `421` => Service not available
        `500`, `501`, `502` => Syntax error
        `530` => User not logged in
        '''
        # Check connection before sending another PASV command
        if self.data_sock and self.data_sock_type:
            addr, port = self.data_sock.getsockname()
            return -1, f'There is already a data connection at {addr} on {port}'

        # Initiate PASV Connection over control socket
        self.sendCmd(f'PASV')
        code, message = self.recvCmd()

        # Handle errors
        if code == 227:
            # Create a new file socket for the passive connection
            expr = re.compile('[0-9]{1,3}')
            pair = expr.findall(message)[1:]  # Removes the code at position 1
            ip = ".".join(pair[0:4])
            p1 = (int(pair[4]) * 256) + int(pair[5])
            self.data_sock_type = 'PASV' # Set the custom PASV flag
            self.data_sock = socket(AF_INET, SOCK_STREAM) # Create IPv4 Socket
            try:
                self.data_sock.connect((ip, p1)) # Connect the file socket
            except:
                return -1, 'There was an error connecting to the Data Transfer Socket'
        
        self.logger.log(code, message)
        return code, message

    def epsv(self, host):
        '''
        Runs the EPSV command (Extended PASV)

        Takes the host from the user and converts it to an IPv6 address to establish a
        connection with the FTP Server. 

        Note: Using `AF_INET6` to signify an IPv6 connection

        Parameters
        ----------
        `host`: str => address of the user's machine (passed in by the system)

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ------------
        `229` => Passive connection established (IPv6!)
        `500`, `501` => Syntax errors 
        
        Note: According to the RFC regarding FTP Extensions, these codes should be sufficient)
        '''
        # Check connection before sending another PASV command
        if self.data_sock and self.data_sock_type:
            addr, port = self.data_sock.getsockname()
            return -1, f'There is already a data connection at {addr} on {port}'

        # Initiate the EPSV Connection over Control socket 
        self.sendCmd(f'EPSV')
        code, message = self.recvCmd()

        if code == 229: # Positive return value
            # Capture the port from the Server's return message
            expr = re.compile('[0-9]{1,6}')
            port = expr.findall(message)
            addr_info = getaddrinfo(host, port[1], AF_INET6)[0][4]
            self.data_sock_type = 'PASV' # Set the PASV flag
            self.data_sock = socket(AF_INET6, SOCK_STREAM)
            try:
                self.data_sock.connect(addr_info) # Create connection
            except:
                self.resetDataSock() # Reset to allow subsequent connections
                return -1, 'There was an error establishing a connection through EPSV'

        self.logger.log(code, message)
        return code, message

    def port(self, host):
        '''
        Runs the PORT command

        Captures the host address from the user and uses it to open a file socket for data transfer
        utilizing the Socket `create_server` helper. The `create_server` helper returns a bound socket 
        that can be used to accept a connection later on in future commands. Setting the `Port` to 0 
        assigns a random/available port from the operating system

        Port Arithmetic: (p1 * 256) + p2 = data port => p1 // 256, p2 % 256 will derive the port values

        `Note: resolves the host with :func:`~FtpClient.handleUserAddressInput``

        Parameters
        ----------
        `host`: str => address to send to the FTP Server for a connection

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ------------
        `200` => Port connected successfully
        `421` => Service unavailable
        `500`, `501` => Syntax or parameter error
        `530` => User not logged in 
        '''
        host = handleUserAddressInput(host)

        filePort = None  # Default to None

        # Open a new file socket for data transfer
        self.data_sock = socket(AF_INET, SOCK_STREAM)
        self.data_sock_type = 'PORT'

        try:
            # Bind to the host and grab the next available port
            self.data_sock = create_server((host, 0))
            _, filePort = self.data_sock.getsockname()  # Capture port from file socket
            # Perform the port arithmetic
            p1 = filePort // 256
            p2 = filePort % 256
            # Create the host and port structure
            send_struct = f'{host.replace(".", ",")},{p1},{p2}'
            self.sendCmd(f'PORT {send_struct}')
        except:
            self.resetDataSock() # Reset to allow subsequent connections
            return -1, f'An error occurred while binding to Host: {host} on Port: {filePort}'

        code, message = self.recvCmd() # Grab final reply
        self.logger.log(code, message)
        return code, message

    def eprt(self, host):
        '''
        Runs the EPRT command

        Captures the host from the user's input and confirms that it will be a valid format. The method
        checks if IPv6 is supported by the machine and then branches into two flows: 1 for IPv6 and 2 for 
        IPv4.

        `IPv6`: Creates an IPv6 Socket and passess the `host` to the socket function `create_server`. This
        function converts the address to an IPv6 address (`family=AF_INET6` param) and then returns that 
        connection to the data socket. The address and port information is then read from the socket and 
        passed to the FTP Server using the EPRT |2| command (IPv6 Support)

        `IPv4`: Creates an IPv4 Socket and passes the `host` to the socket function `create_server`. No
        conversions are needed for this call. The address and port information is then read from the socket
        and passed to the FTP Server using the EPRT |1| command (IPv4 Support)

        Note: When running on IPv4 with this method, the port arithmetic is not required, since the updated
        sending structure supports sending over the full port.

        Parameters
        __________
        `host`: str => the user's host so that it can send it to the FTP Server for the connection

        Return
        ______
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ____________
        `200` => Successful command receipt
        `500`, `501` => Sytnax errors

        Note: According to the RFC regarding FTP Extensions, these codes should be sufficient)
        '''
        addr = filePort = send_struct = None
        host = handleUserAddressInput(host)

        try:
            if has_dualstack_ipv6():
                self.data_sock = socket(AF_INET6, SOCK_STREAM)
                self.data_sock_type = 'PORT'
                # Create server takes the IPv4 Address and converts it to an IPv6 Address
                self.data_sock = create_server((host, 0), family=AF_INET6, dualstack_ipv6=True)
                self.logger.log('INFO', 'IPv6 is supported by this machine')
                addr, filePort, *_ = self.data_sock.getsockname()
                # Create the EPRT address structure (IPv6)
                send_struct = f'|2|{addr}|{filePort}|'
            else:
                self.data_sock_type = 'PORT'
                self.data_sock = socket(AF_INET, SOCK_STREAM)
                # Create a fall-back server with an IPv4 Socket
                self.data_sock = create_server((host, 0))
                self.logger.log('INFO', 'IPv6 is NOT supported by this machine, switching to IPv4')
                addr, filePort= self.data_sock.getsockname()
                # Create the EPRT address structure (IPv4 Fall-back)
                send_struct = f'|1|{host}|{filePort}'
            self.sendCmd(f'EPRT {send_struct}')
        except:
            self.resetDataSock()
            return -1, f'An error occurred while binding to Host: {host} on Port: {filePort}'

        code, message = self.recvCmd()
        self.logger.log(code, message)
        return code, message

    def retr(self, pathname):
        '''
        Runs the RETR command

        Takes in the pathname of the desired file to acquire and passes it to the FTP Server. The
        server then sends the data as binary data through the data socket. This method reads that 
        information in as a readonly binary file and writes it out to a file. This method allows 
        for the transmission of various file types (PDFs, XLSX, TXT, etc.)

        Parameters
        ----------
        `pathname`: str => path of the file that should be retrieved from the FTP Server

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ------------
        `-1` => Path to file not provided
        `110` => Restart marker (MARK yyyy = mmmm)
        `125`, `150` => Good command, transfer starting
        `226`, `250` => Closing connection 
        `421`, `425`, `426`, `450`, `451` => Error connecting; Request action not taken
        `500`, `501` => Syntax or command error
        `530` => User is not logged in
        `550` => File unavailable for transfer; action not taken
        '''
        if not pathname:
            return -1, 'Please provide the name of the file you would like to retrieve.'

        self.sendCmd(f'RETR {pathname}')
        code, message = self.recvCmd()
        if code == 125 or code == 150:
            self.logger.console_log(code, message)
            self.data_sock.settimeout(DEFAULT_DATA_TIMEOUT)
            conn = self.data_sock
            if self.data_sock_type == 'PORT':
                conn, _ = self.data_sock.accept()
            # Treats the socket like a binary readonly file
            with conn.makefile('rb') as data:
                # Writes the binary data to the file (Accepts PDFs)
                with open(pathname, 'wb') as local_file:
                    for line in data:
                        local_file.write(line)

            code, message = self.recvCmd()

        self.logger.log(code, message)
        self.resetDataSock() # Reset to allow subsequent connections
        return code, message

    def stor(self, pathname):
        '''
        Runs the STOR command

        Sends the desired file name to the FTP Server so that it can open up the file on 
        the server for writing. The method then passes the data to the server using the 
        `:func:sendFile()` method, which utilizes the `:func:sendfile` method to transfer
        the data as a binary file to the FTP Server. 

        Parameters
        ----------
        `pathname`: str => pathname of the file to store in the FTP Server

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ------------
        `110` =>
        `125`, `150` =>
        `226`, `250` =>
        `421`, `425`, `426`, `450`, `451`, `452` =>
        `500`, `501` =>
        `530` => User is not logged in
        '''
        if not pathname:
            return -1, 'Please provide the name of the file you would like to transfer.'

        if not os.path.exists(pathname):
            return -5, 'Please provide a valid file name'

        self.sendCmd(f'STOR {pathname}')
        code, message = self.recvCmd()
        if code == 125 or code == 150:
            self.logger.console_log(code, message)
            size = self.sendFile(pathname)
            if not size:
                return size, 'Error while sending file'
            code, message = self.recvCmd()

        self.logger.log(code, message)
        self.resetDataSock()
        return code, message

    def pwd(self):
        '''
        Runs the PWD command

        Returns the present working directory of the FTP Server

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ------------
        `257` => Pathname created
        `421` => Service not available
        `500`, `501`, `502` => Syntax error
        `550` => File unavailable; requested action not taken
        '''
        self.sendCmd(f'PWD')
        pwd_code, pwd_message = self.recvCmd()

        self.logger.log(pwd_code, pwd_message)
        return pwd_code, pwd_message

    def syst(self):
        '''
        Runs the SYST command

        Returns the current system information from the FTP Server

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ------------
        `215` => Name of the system
        `421` => Service not available
        `500`, `501`, `502` => Syntax error
        '''
        self.sendCmd(f'SYST')
        syst_code, syst_message = self.recvCmd()

        self.logger.log(syst_code, syst_message)
        return syst_code, syst_message

    def list(self, pathname=''):
        '''
        Runs the LIST command

        Passes the pathname (value or blank) to the FTP Server and captures
        the reply. Using the socket `makefile` method, the output of the socket
        is directed to the temporary `data` file pointer. After the transmission, the 
        `data socket` is reset so that the reference is not left dangling.

        Parameters
        ----------
        `pathname`: std => location to run LIST (defaults to an empty string)

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ------------
        `125`, `150` => Good command; transfer starting\n
        `226`, `250` => Closing connection\n
        `425`, `426`, `450`, `451` => Error connecting; Request action not taken\n
        `500`, `501`, `502` => Syntax or command error\n
        `530` => User not logged in\n
        '''
        self.sendCmd(f'LIST {pathname}')
        response_code, response_message = self.recvCmd()

        if response_code == 150 or response_code == 125:
            self.logger.console_log(response_code, response_message)
            conn = self.data_sock
            if self.data_sock_type == 'PORT':
                try:
                    conn, _ = self.data_sock.accept()
                except:
                    self.logger.log('ERROR', 'There was an error while accepting the connection.')
            # Convert the socket to a read-only file to handle the data
            try:
                with conn.makefile('r') as data:
                    for line in data:
                        # Remove dangling newline character
                        line = line.strip('\n')
                        self.logger.console_log('Received', line)
            except timeout:
                self.logger.log('WARNING', 'LIST command timedout')
            except error:
                self.logger.log('ERROR', 'List command encountered an error')
            # Capture the final return command
            response_code, response_message = self.recvCmd()

        if response_code > 400 and response_code < 500:
            self.logger.log(
                response_code, f'Checking Sockets | Control: {self.checkControlSock} Data: {self.checkDataSock}')

        self.logger.log(response_code, response_message)
        self.resetDataSock()  # Reset the data socket after transmission
        return response_code, response_message

    def help(self, option=''):
        '''
        Runs the HELP command

        Returns the help menu from the FTP Server

        Parameters
        ----------
        `option`: str => command for help

        Return
        ------
        `code`: int => code returned from the control socket
        `message`: str => message returned from the control socket

        Return Codes
        ------------
        `211`, `214` => Good return\n
        `421` => Service not available\n
        `500`, `501`, `502` => Syntax or command error\n
        `530` => User not logged in\n
        '''
        self.sendCmd(f'HELP {option.upper()}')
        response_code, response_message = self.recvCmd()
        self.logger.console_log(response_code, response_message)
        if response_code == 214:
            self.logger.console_log('INFO', 'These commands are recognized by the FTP Client:')
            for helpCommand in self.helpCommands:
                self.logger.console_log('-', helpCommand)
            self.logger.console_log('INFO', 'These commands are the default FTP Commands:')
            while True:
                help_code, help_message = self.recvCmd()
                if help_code == 214:
                    return help_code, help_message
                else:
                    self.logger.console_log(help_code, help_message)
        else:
            self.logger.log(response_code, response_message)
            return response_code, response_message
