#!/usr/bin/python3

# CS 472 - HW2
# Christopher S. Good, Jr. 
# Logger.py

import datetime
import os

class Logger: 
    '''
    Python Logger
    '''
    def __init__(self, file='ftp-logger.txt', mode='a'):
        '''
        Initializes the Python Logger

        Parameters
        ----------
        `file`: str => filename to print logs (defaults to 'ftp-logger.txt')
        '''
        self.logFile = open(file, mode) # Defaults to append

    def __del__(self):
        '''
        Destructs the log file when this class leaves scope
        '''
        self.logFile.close()

    def log_format(self, level, message):
        '''
        Generates the formatted string for the Logger Output

        `NOTE: Logger levels are based off of the FTP Protocol Return Codes`

        Parameters
        ----------
        `level`: str | int => level of the output (Info, Warning, Error, etc.)
        `message`: str => information that the user would like to display/log

        Return
        ------
        `formatted_message`: str => general logger format for the project M/D/Y H:M:S LEVEL MESSAGE
        '''
        formatted_message = None
        d = datetime.datetime.now().strftime(format="%m/%d/%y %H:%M:%S")
        if level == None:
            formatted_message = f'{d} ERROR {message}'
        elif type(level) == str:
            formatted_message = f'{d} {level}: {message}'
        elif level < 220:
            formatted_message = f'{d} INFO {message}'
        elif level < 299:
            formatted_message = f'{d} SUCCESS {message}'
        elif level < 399:
            formatted_message = f'{d} PENDING {message}'
        elif level < 499:
            formatted_message = f'{d} WARNING {message}'
        elif level < 599:
            formatted_message = f'{d} ERROR {message}'
        elif level < 650:
            formatted_message = f'{d} CONFIDENTIAL {message}'
        else: 
            formatted_message = f'{d} XXXXX {message}'
        return formatted_message

    def screen_log(self, level, message):
        '''
        Logs the given message to the screen
        
        `NOTE: Will not update the log file!`

        Parameters
        ----------
        `level`: str | int => level of the output (Info, Warning, Error, etc.)
        `message`: str => information that the user would like to display
        '''
        print(self.log_format(level, message))

    def console_log(self, level, message):
        '''
        Logs the given message to the screen and the log file

        `NOTE: This is the most VERBOSE option`

        Parameters
        ----------
        `level`: str | int => level of the output (Info, Warning, Error, etc.)
        `message`: str => information that the user would like to display & log
        '''
        self.log(level, message)
        print(message)

    def log(self, level, message):
        '''
        Logs the given message to the log file only

        Parameters
        ----------
        `level`: str | int => level of the output (Info, Warning, Error, etc.)
        `message`: str => information that the user would like to display/log
        '''
        self.logFile.write(self.log_format(level, message + '\n'))

    def clean_log(self):
        '''Helper function that safely cleans the log file'''
        if os.path.exists(self.logFile.name):
            self.logFile.close()
            os.remove(self.logFile.name)
        else:
            print('Could not remove log file...')