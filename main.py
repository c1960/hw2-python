#!/usr/bin/python3

# CS 472 - HW2
# Christopher S. Good, Jr. 
# main.py

import sys
from Logger import Logger
from FtpClient import FtpClient
from AddressHandler import handleUserAddressInput

def handleUserInput(message):
    '''Helper function that handles the user's input or quits with the command phrase/trigger'''
    input_value = None
    try:
        input_value = input(message)
        if input_value.upper() == 'QUIT':
            return 221 # Closing control connection
        return input_value
    except KeyboardInterrupt:
        print() # Blank print to move the stdout down a line after ^C is hit
        return 221 # Closing constrol connection
    except: 
        return -1

def handleLogIn(client, host, port):
    '''
    Captures the user's login and password credentials and feeds them to 
    the FTP Server - the responses are the codes returned from USER and PASS commands
    Note: The password will have a value of 500+ (or be < 0) if it fails!
    '''
    username_code = password_code = 0
    while username_code not in [230, 331] or password_code != 230:

        username = handleUserInput(f'User ({host}, {port}): ')
        if username == 221:
            return username

        username_code, _ = client.user(username)

        password = handleUserInput(f'Password: ')
        if password == 221:
            return password
        
        password_code, _ = client.password(password)

        if password_code == 530:
            return password_code
        
    return password_code

def handleCommandInput():
    '''
    Destructures the user's input

    Separates the user's command from their arguments and handles exceptions when 
    arguments aren't passed. This is designed to make the main control flow easier to read
    and more logical (Also better practice when handling errors)

    Return
    ------
    `command`: str => user's command
    `arg`: str => user's argument
    '''
    command = input(f'% ftp % ')
    command = command.strip()  # Handle prepended spaces
    command = command.split(' ', 1)
    if len(command) == 2:
        return command[0], command[1]
    return command[0], None

def handleCommandLineInput():
    '''
    Helper that grabs the arguments from the command line and parses them

    Returns
    -------
    `addr`: str => Address captured from the `-a` flag
    `logfile`: str => File name captures from the `-lf` flag
    '''
    ip_addr = logfile = None

    if '-a' in sys.argv:
        try: 
            ip_addr_index = sys.argv.index('-a')
            ip_addr = handleUserAddressInput(sys.argv[ip_addr_index + 1])
        except:
            print('Could not find a valid address argument')

    if '-lf' in sys.argv:
        try:
            lf_index = sys.argv.index('-lf')
            logfile = sys.argv[lf_index + 1]
        except:
            print('Could not find a valid log file argument')

    return ip_addr, logfile

if __name__ == "__main__":
    '''
    Main function loop for the program

    Takes in commandline arguments that follow specific flags:
    `-a`: str => Address of the FTP Server to connect to
    `-lf`: str => Name of the file to store logging information

    This function handles the main control flow of the application. After parsing the 
    commandline arguments, it will establish a connection to the FTP Server and prompt
    the user for a `USERNAME` and `PASSWORD`. After the user is logged in, the program will
    run infinitely until the user enters the phrase 'quit' (any caps) or hits ^C on the 
    keyboard.

    The main logic statement interacts with the FTP Client Wrapper that will handle the 
    actual communication with the Server (All sockets and connections are handled there)

    Note: The system will not run well if the user does not enter at least an address
    '''

    logger = client = ip_addr = logged_in = None

    # Confirm proper user input
    ip_addr, logfile = handleCommandLineInput()

    # Init the logger
    if logfile:
        logger = Logger(logfile)
    else:
        logger = Logger() # Default Logger
    
    if ip_addr:
        # Init the client with Logger
        client = FtpClient(logger)

        try: # Attempt to use 
            client.connect(ip_addr)
        except:
            logger.console_log('ERROR', f'There was an error establishing the connection to {ip_addr}')

        # Handle the Log-in Control Flow
        logged_in = handleLogIn(client, ip_addr, port=21)

    if logged_in == 221:
        code, message = client.quit()
        logger.console_log(code, message)
        sys.exit(0)

    # Main Control Flow
    while True:
        command = code = message = None
        try:
            command, arg = handleCommandInput()
            # QUIT/EXIT: Leave the application
            if (command.upper() == 'QUIT' or command.upper() == 'EXIT'):
                code, message = client.quit()
            # FQUIT: Force quit the application 
            elif command.upper() == 'FQUIT':
                code, message = 0, 'FORCE QUIT'
            # USER/USERNAME: Send the Username to the Server
            elif (command.upper() == 'USER' or command.upper() == 'USERNAME') and arg:
                code, message, = client.user(arg)
            # PASS/PASSWORD: Send the Password to the Server
            elif (command.upper() == 'PASS' or command.upper() == 'PASSWORD') and arg:
                code, message = client.password(arg)
            # CWD/CD: Change the working directory
            elif (command.upper() == 'CWD' or command.upper() == 'CD') and arg:
                code, message = client.cwd(arg)
            # PORT: Assign an addr/port for the Server to connect to 
            elif (command.upper() == 'PORT') and arg:
                code, message = client.port(arg)
            # EPORT/EXTENDEDPORT: Assign an IPv6 addr/port for the Server to connect to 
            elif (command.upper() == 'EPRT' or command.upper() == 'EXTENDEDPORT') and arg:
                code, message = client.eprt(arg)
            # EPSV/EXTENDEDPASSIVE: Acquire an IPv6 addr/port from the server
            elif (command.upper() == 'EPSV' or command.upper() == 'EXTENDEDPASSIVE'):
                code, message = client.epsv(ip_addr)
            # PASV/PASSIVE: Acquire an addr/port from the server
            elif (command.upper() == 'PASV' or command.upper() == 'PASSIVE'):
                code, message = client.pasv()
            # RETR/GRAB: Download a file from the Server
            elif (command.upper() == 'RETR' or command.upper() == 'GRAB') and arg:
                code, message = client.retr(arg)
            # LIST/LS: Print out the current contents of the working directory
            elif (command.upper() == 'LIST' or command.upper() == 'LS'):
                if arg:
                    code, message = client.list(arg) # Accept and argument
                else:
                    code, message = client.list()
            # STOR/PUT: Upload a file to the Server
            elif (command.upper() == 'STOR' or command.upper() == 'PUT') and arg:
                code, message = client.stor(arg)
            # SYST/SYSTEM: Capture the system's information/specs
            elif (command.upper() == 'SYST' or command.upper() == 'SYSTEM'):
                code, message = client.syst()
            # PWD: Print the working directory (This name was solid)
            elif command.upper() == 'PWD':
                code, message = client.pwd()
            # HELP: Print the commands available to the user (This name was solid)
            elif command.upper() == 'HELP':
                code, message = client.help()
            # RESET: Convenience function that resets the data socket connections/flags
            elif command.upper() == 'RESET':
                code, message = client.resetDataSock()
            # LOGIN: Convenience function that helps the user log back in without the USER/PASS commands
            elif command.upper() == 'LOGIN':
                if logged_in == 230:
                    logger.console_log('INFO', 'User is already logged in!')
                else:
                    logged_in = handleLogIn(client, ip_addr, port=21)
                    if logged_in == 221:
                        code, message = client.quit()
            else:
                logger.console_log('WARNING', f'Improper command "{command}", please try again!')
                continue # Pass over the reply
        except KeyboardInterrupt: # Keyboard interruption will allow the user to exit the program 
            print() # Blank print to move the stdout down a line after ^C is hit
            logger.console_log('INFO', 'Goodbye!')
            break
        except: # Blanket excpetion to catch all other issues
            logger.console_log('WARNING', f'Something went wrong while entering the command {command}, please try again!')
            continue

        # Custom Error Codes/Handling
        if code == -5: # File Error => Error while reading/writing to a file
            logger.screen_log('ERROR', f'File Error: {message}')
        elif code == -4: # Socket Error => Connection to the socket was terminated
            logger.screen_log('ERROR', f'Socket Connection Terminated: {message}')
        elif code == -3: # Command Error => The command needed more parameters or information
            logger.screen_log('ERROR', f'The command "{command} requires more input: {message}')
        elif code == -2: # Socket Error => The socket timed out
            logger.screen_log('ERROR', f'The command "{command}" timed out: {message}')
        elif code == -1: # General Error => Openended error message template
            logger.screen_log('ERROR', f'The command "{command}" returned: {message}')
        elif code == 0 and message == 'FORCE QUIT': # Force quit the application (Designed for lack of IP Address)
            logger.console_log('WARNING', f'Force quitting the application with {command}')
            break
        elif code == 221: # Closing control socket 
            logger.screen_log(code, message)
            break
        elif code and message: # Display Client return codes
            logger.screen_log(code, message)

    sys.exit(0) # Exit successfully